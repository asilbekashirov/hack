import { useState } from "react";

export default function useToggle(initialState: boolean) {
    const [state, setState] = useState(initialState)

    const on = () => setState(true)
    const off = () => setState(false)
    const toggle = () => setState(!state)

    return {state, on, off, toggle}

}