import { Stack, Typography, useMediaQuery, useTheme } from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import { motion, useCycle } from "framer-motion";
import useToggle from "../../hooks/useToggle";
import { useRef } from "react";
import { useDimensions } from "../../hooks/useDimension";
import { Navigation } from "./MenuList";
import { MenuToggle } from "./MenuToggle";

const links = [
  {
    label: "AI Chat",
    href: "/ai-chat",
  },
  {
    label: "Face Recognition",
    href: "/face-recognition",
  },
  {
    label: "Voice Assistant",
    href: "/voice-assistant",
  },
  {
    label: "Sign Language",
    href: "/sign-language",
  },
  {
    label: "Health Management",
    href: "/health-management",
  },
];

const sidebar = {
  open: (height = 1000) => ({
    clipPath: `circle(${height * 2 + 200}px at 40px 40px)`,
    transition: {
      type: "spring",
      stiffness: 20,
      restDelta: 2,
    },
  }),
  closed: {
    clipPath: "circle(30px at 40px 40px)",
    transition: {
      delay: 0.5,
      type: "spring",
      stiffness: 400,
      damping: 40,
    },
  },
};

const Navbar = () => {
  const location = useLocation();

  const isLanding = location.pathname === "/landing";

  // const [isOpen, toggleOpen] = useCycle(false, true);
  // const containerRef = useRef<HTMLDivElement | null>(null);
  // const { height } = useDimensions(containerRef);

  const theme = useTheme();

  const lg = useMediaQuery(theme.breakpoints.down("lg"));

  return (
    <Stack
      display="flex"
      justifyContent="center"
      sx={{
        bgcolor: "#f3f3f380",
        width: "100%",
        height: "5rem",
        position: "fixed",
        backdropFilter: "blur(15px)",
      }}
    >
      <Stack
        display="flex"
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        sx={{
          margin: "0 auto",
          maxWidth: "1500px",
          width: "100%",
          px: isLanding ? (lg ? 5 : 10) : 0,
        }}
      >
        {lg && <span></span>}
        <Typography component="h5" variant="h5">
          Empower AI
        </Typography>
        {lg ? (
          <></>
          // <motion.nav
          //   ref={containerRef}
          //   custom={height}
          //   animate={isOpen ? "open" : "closed"}
          //   initial={false}
          //   // className="flex"
          // >
          //   <motion.div className="menu-background" variants={sidebar} />
          //   <Navigation lg={lg} />
          //   <MenuToggle toggle={() => toggleOpen()} />
          // </motion.nav>
        ) : (
          <div className="links">
            {links.map((link) => (
              <Link className="link" to={link.href}>
                {link.label}
              </Link>
            ))}
          </div>
        )}
      </Stack>
    </Stack>
  );
};

export default Navbar;
