import { useRef } from "react"
import { useAudio } from "./useAudio"
import './audio.scss'

const AudioVisualizer = () => {

    const containerRef = useRef<HTMLDivElement | null>(null)

    useAudio({container: containerRef})

    return (
        <div ref={containerRef} className="audio-wrapper">

        </div>
    )
}

export default AudioVisualizer