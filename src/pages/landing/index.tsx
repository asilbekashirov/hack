import { Card, Grid, Stack, Typography } from "@mui/material";
import { Icon } from "@iconify/react";
import chatgpt from "../../images/chatgpt.png";
import phone from "../../images/main.png";

const LandingPage = () => {
  return (
    <Stack display="flex" direction="column">
      <Grid container className="screen">
        <Grid item md={6} sm={12} className="flex col jcc aic">
          <Stack display="flex" direction="column" gap={5}>
            <Typography
              variant="h1"
              component="h1"
              sx={{ fontSize: "5rem" }}
              className="font-600"
            >
              Embrace <br /> inclusivity and <br /> unlock new <br />{" "}
              possibilities
            </Typography>
            <Typography sx={{mt: 4}} variant="h3" component="h3" className="font-200">
              With EmpowerAI
            </Typography>
            <Typography
              variant="h5"
              component="h5"
              color="#00000060"
              className="flex aic"
            >
              Start the journey{" "}
              <Icon className="ml-1" icon="ic:outline-arrow-downward" />
            </Typography>
          </Stack>
        </Grid>
        <Grid item md={6} sm={12} sx={{justifyContent: 'flex-end'}} className="flex aic">
          <img src={phone} />
        </Grid>
      </Grid>
      <Stack direction="column">
        <Card elevation={3} sx={{ bgcolor: "white", borderRadius: 6 }}>
          <Grid container>
            <Grid item md={7}>
              <Stack direction="column" sx={{ p: 7 }}>
                <Typography variant="h4" component="h4">
                  AI Chat
                </Typography>
                <Typography variant="body1" sx={{maxWidth: '70%', mt: 5}}>
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Aliquid, nulla illum est tenetur vitae officia cupiditate
                  optio, neque, fuga aperiam laboriosam tempore et accusamus
                  architecto! Recusandae vel nobis quas autem sunt neque
                  deserunt, molestias id ipsa iste alias eveniet eius voluptas
                  dignissimos illo, modi iusto! Inventore vel repudiandae
                  aperiam, doloribus debitis ad repellendus quam magnam maiores,
                  tempora consectetur natus laborum, laboriosam labore cum quos
                  voluptate et tempore numquam totam? Dolor, excepturi et esse
                  optio animi provident quam blanditiis ipsum. Cumque ratione
                  voluptates nemo possimus veniam necessitatibus provident sed,
                  odit minus iusto tempore ut illo nisi? Quam id tenetur officia
                  labore.
                </Typography>
                <Typography textAlign="right" variant="body1" sx={{maxWidth: '70%', cursor: 'pointer', textDecoration: 'underline'}}>
                    See more
                </Typography>
              </Stack>
            </Grid>
            <Grid item md={5}>
              <img src={chatgpt} className="image-full" />
            </Grid>
          </Grid>
        </Card>
      </Stack>
    </Stack>
  );
};

export default LandingPage;
