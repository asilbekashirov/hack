import { useRoutes } from "react-router-dom";
import MainLayout from "./components/layout/MainLayout";
import LandingPage from "./pages/landing";
import GestureRecognition from "./components/gestureRecognition/GestureRecognition";

export function Router() {
  return useRoutes([
    {
      path: "/",
      element: <MainLayout />,
      children: [
        {
          path: "/",
          element: <LandingPage />,
        },
        {
          path: "/gesture",
          element: <GestureRecognition />,
        },
      ],
    },
  ]);
}
