import { Box, Stack } from "@mui/material"
import { Outlet, useLocation } from "react-router-dom"
import Navbar from "./Navbar"

const MainLayout = () => {

    const location = useLocation()

    const isLanding = location.pathname === "/landing"

    return (
        <Stack display="flex" direction="column">
            <Navbar />
            <Box sx={{mt: '5rem', px: isLanding ? 10 : 0, maxWidth: '1500px', margin: '0 auto'}}>
                <Outlet />
            </Box>
        </Stack>
    )
}

export default MainLayout