import { useEffect, useRef } from "react";

interface IProps {
  container: React.MutableRefObject<HTMLDivElement | null>;
  num?: number;
}

const width = 10;

export function useAudio({ container, num = 32 }: IProps) {
  const contextRef = useRef<AudioContext | null>(null);
  const analyzer = useRef<AnalyserNode | null>(null);

  const array = new Uint8Array(num * 2);

  function handleClick() {
    if (contextRef.current) return;

    for (let i = 0; i < num; i++) {
      const bar = document.createElement("div");
      bar.classList.add("bar");
      bar.style.background = "#348feb";
      bar.style.minWidth = `${width}px`;
      container.current?.appendChild(bar);
    }

    contextRef.current = new AudioContext();
    analyzer.current = contextRef.current.createAnalyser();

    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then((stream) => {
        const src = contextRef.current?.createMediaStreamSource(stream);
        src?.connect(analyzer.current as AnalyserNode);
        loop();
      })
      .catch((err) => console.log(err));
  };

  function loop() {
    window.requestAnimationFrame(loop);
    analyzer.current?.getByteFrequencyData(array);

    const myElements = document.getElementsByClassName(
      "bar"
    ) as HTMLCollectionOf<HTMLDivElement>;

    for (let i = 0; i < num; i++) {
      const height = array[i + num];
      myElements[i].style.minHeight = `${height}px`;
      myElements[i].style.opacity = `${0.008 * height}`;
    }
  };

  useEffect(() => {
    container.current?.addEventListener("click", handleClick);
  }, [container]);
}
